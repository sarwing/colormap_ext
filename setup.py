from setuptools import setup 

setup(name='colormap_ext',
      description='Loads a set of external cmaps and register them to make them available through matplotlib.cm.get_cmap or pyplot.get_cmap',
      url='https://git.cersat.fr/oarcher/colormap_ext.git',
      author = "Theo Cevaer",
      author_email = "Theo.Cevaer@ifremer.fr",
      use_scm_version={'write_to': '%s/_version.py' % "colormap_ext"},
      setup_requires=['setuptools_scm'],
      license='GPL',
      packages=['colormap_ext'],
      zip_safe=False,
      include_package_data=True,
      #package_data={'palettes': ['palettes/*',]}
      #data_files=[('colormap_ext', ['palettes/*'])]
)
