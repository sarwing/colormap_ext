# Charger une liste de palettes externe, les transformer en cmap et faire des matplotlib.cm.register_cmap
# Une fonction qui ,a partir d'une palette permet d'obtenir un cmap
# Les palettes doivent être inscrites dans le setup.py.
# Fonction pour obtenir la liste des palettes externes
import os
import logging
import sys
import matplotlib.colors as mpcolors
import matplotlib.cm as cm
import glob
import numpy
import colorsys

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)


def getColorMap(rgbFile):
    '''
    Load a RGB palette provided in ascii file
    '''
    colors = []
    nbCol=0

    for line in rgbFile:
        r,g,b = [int(c) for c in line.split()]
        colors.append( [r/255.,g/255.,b/255.] )
        nbCol += 1
    return( mpcolors.ListedColormap(colors, name="custom", N=nbCol) )


def registerPal(rgbFilepath):    
    try:
        f = open(rgbFilepath)
    except:
        logger.error("Couldn't open rgb file %s. The palette will not be added.")
        return False
    cmap = getColorMap(f)
    cm.register_cmap(name=os.path.splitext(os.path.basename(rgbFilepath))[0], cmap=cmap)
    cmapList.append(os.path.splitext(os.path.basename(rgbFilepath))[0])
    return True


def getExtCmapList():
    return cmapList


def load_cpt_faozi(fileName):
    """
    Originally from http://wiki.scipy.org/Cookbook/Matplotlib/Loading_a_colormap_dynamically
    Modifications :
     - move imports outside function
     - replace Numeric by numpy
     - remove GMTPath argument, fileName is now the complete path of ctp file
     - file opening
    """
    with open(fileName) as f:
        lines = f.readlines()

    x = []
    r = []
    g = []
    b = []
    colorModel = "RGB"
    for line in lines:
        ls = line.split()
        if line[0] == "#":
            if ls[-1] == "HSV":
                colorModel = "HSV"
                continue
            else:
                continue
        if ls[0] == "B" or ls[0] == "F" or ls[0] == "N":
            pass
        else:
            x.append(float(ls[0]))
            r.append(float(ls[1]))
            g.append(float(ls[2]))
            b.append(float(ls[3]))
            xtemp = float(ls[4])
            rtemp = float(ls[5])
            gtemp = float(ls[6])
            btemp = float(ls[7])

    x.append(xtemp)
    r.append(rtemp)
    g.append(gtemp)
    b.append(btemp)

    nTable = len(r)
    x = numpy.array(x)
    r = numpy.array(r)
    g = numpy.array(g)
    b = numpy.array(b)
    if colorModel == "HSV":
        for i in range(r.shape[0]):
            rr,gg,bb = colorsys.hsv_to_rgb(r[i] / 360., g[i], b[i])
            r[i] = rr
            g[i] = gg
            b[i] = bb
    if colorModel == "HSV":
        for i in range(r.shape[0]):
            rr,gg,bb = colorsys.hsv_to_rgb(r[i] / 360., g[i], b[i])
            r[i] = rr
            g[i] = gg
            b[i] = bb
    if colorModel == "RGB":
        r = r / 255.
        g = g / 255.
        b = b / 255.
    xNorm = (x - x[0]) / (x[-1] - x[0])

    red = []
    blue = []
    green = []
    for i in range(len(x)):
        red.append([xNorm[i], r[i], r[i]])
        green.append([xNorm[i], g[i], g[i]])
        blue.append([xNorm[i], b[i], b[i]])
    colorDict = {"red":red, "green":green, "blue":blue}
    return colorDict


def dictToColormap(colorDict, name="custom_cpt"):
    red = colorDict["red"]
    green = colorDict["green"]
    blue = colorDict["blue"]
    cdict = {'red': red, 'green': green, 'blue': blue}
    return mpcolors.LinearSegmentedColormap(name, segmentdata=cdict, N=256)


def load_pal(palettes_files):
    cmapList = []
    for path_palette in palettes_files:
        with open(path_palette, 'r') as pal:
            cmap = getColorMap(pal)
            cm.register_cmap(cmap=cmap, name=os.path.splitext(os.path.basename(path_palette))[0])
            cmapList.append(os.path.splitext(os.path.basename(path_palette))[0])
    return cmapList


def load_cpt(cpt_files):
    cmapList = []
    for file in cpt_files:
        colorDict = load_cpt_faozi(file)
        cptCmap = dictToColormap(colorDict, name=os.path.splitext(os.path.basename(file))[0])
        cm.register_cmap(cmap=cptCmap, name=cptCmap.name)
        cmapList.append(cptCmap.name)
    return cmapList


cmapList = []
dirPath = os.path.join(__path__[0], "palettes/")
if not os.path.isdir(dirPath):
    palettes = glob.glob("%s/palettes/*.pal" % sys.prefix)
else:
    palettes = glob.glob(os.path.join(dirPath, "*.pal"))

cmapList.extend(load_pal(palettes))

if not os.path.isdir(dirPath):
    cpt = glob.glob("%s/palettes/*.cpt" % sys.prefix)
else:
    cpt = glob.glob(os.path.join(dirPath, "*.cpt"))

cmapList.extend(load_cpt(cpt))
